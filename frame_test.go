package okoframe

import (
	"fmt"
	"path/filepath"
	"testing"
)

func TestBuildFrame(t *testing.T) {
	blankFrame := Frame{}
	testFrame := Frame{
		" ",
		0,
		0,
		0,
		0,
		0,
		[]byte{0x0},
	}

	//t.Errorf("Couldn't build blank frame. ")

	images, _ := filepath.Glob("testArticles/*")
	for _, img := range images {
		cvt := BuildFrameFromImage(img)
		fmt.Println(cvt.Id)
	}

	fmt.Println(testFrame, blankFrame)
}

func TestSaveUnsaveFrame(t *testing.T) {
	images, _ := filepath.Glob("testArticles/*")
	for _, img := range images {
		cvt := BuildFrameFromImage(img)
		name, err := cvt.SaveToDisk()
		if err != nil {
			fmt.Println("Error: ", err)
		}

		fmt.Println("Reading ", name)
		readFrame, readErr := ReadFromDisk(name)
		if readErr != nil {
			fmt.Println("Read failure: ", readErr)
		}

		fmt.Println(readFrame.ChannelDepth)
		fmt.Println(readFrame.Id)
	}
}
