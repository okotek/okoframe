package okoframe

import "math"

type Pixel struct {
	Chans [4]uint8 //If there is only one channel, GBA is left empty. Similarly, if Alpha is missing, we get RGB[0]
	XPos  uint64   ``
	YPos  uint64
}

func byteDiff(bta, btb uint8) float64 {
	return math.Abs(float64(bta) - float64(btb))
}

func (px Pixel) getSpecificChanDiff(ref Pixel, chans [4]bool) []uint8 {
	retSlc := []uint8{}

	var rDiff uint8 = uint8(byteDiff(px.Chans[0], ref.Chans[0]))
	var gDiff uint8 = uint8(byteDiff(px.Chans[1], ref.Chans[1]))
	var bDiff uint8 = uint8(byteDiff(px.Chans[2], ref.Chans[2]))
	var aDiff uint8 = uint8(byteDiff(px.Chans[3], ref.Chans[3]))

	for i := 0; i < 4; i++ {
		if i == 0 && chans[i] == true {
			retSlc = append(retSlc, rDiff)
		} else if i == 1 && chans[i] == true {
			retSlc = append(retSlc, gDiff)
		} else if i == 2 && chans[i] == true {
			retSlc = append(retSlc, bDiff)
		} else if i == 3 && chans[i] == true {
			retSlc = append(retSlc, aDiff)
		} /*else {
			retSlc = append(retSlc, 0)
		}

		*/
	}

	return retSlc
}

func (px Pixel) getDiffs(ref Pixel) (uint8, uint8, uint8, uint8) {
	var rDiff uint8 = uint8(byteDiff(px.Chans[0], ref.Chans[0]))
	var gDiff uint8 = uint8(byteDiff(px.Chans[1], ref.Chans[1]))
	var bDiff uint8 = uint8(byteDiff(px.Chans[2], ref.Chans[2]))
	var aDiff uint8 = uint8(byteDiff(px.Chans[3], ref.Chans[3]))

	return rDiff, gDiff, bDiff, aDiff

}

func (px Pixel) getAvgDiffs(ref Pixel) uint8 {
	pxCount := 0
	rollingAccumulator := 0

	var rDiff uint8 = uint8(byteDiff(px.Chans[0], ref.Chans[0]))
	var gDiff uint8 = uint8(byteDiff(px.Chans[1], ref.Chans[1]))
	var bDiff uint8 = uint8(byteDiff(px.Chans[2], ref.Chans[2]))
	var aDiff uint8 = uint8(byteDiff(px.Chans[3], ref.Chans[3]))

	if rDiff != 0 {
		rollingAccumulator += int(rDiff)
		pxCount++
	}

	if gDiff != 0 {
		rollingAccumulator += int(gDiff)
		pxCount++
	}

	if bDiff != 0 {
		rollingAccumulator += int(bDiff)
		pxCount++
	}

	if aDiff != 0 {
		rollingAccumulator += int(aDiff)
		pxCount++
	}

	avgVal := uint8(rollingAccumulator / pxCount)
	return avgVal

}
