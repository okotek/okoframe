package okoframe

import (
	"fmt"
	"image"
)

func (fr Frame) GetPixels() []Pixel {

	var pxRet []Pixel = []Pixel{}
	var xCounter uint64 = 0
	var yCounter uint64 = 0
	var channelCount uint64 = 0

	for i := 0; i < len(fr.PixelBytes); i += int(fr.ChannelDepth) {
		tmpPxl := Pixel{}
		tmpPxl.XPos = xCounter
		tmpPxl.YPos = yCounter

		//We can't use appending here because of fixed array. Need to keep track of channel iteration
		for pxi := 0; pxi < int(fr.ChannelDepth); pxi++ {
			tmpPxl.Bytes = append(tmpPxl.Bytes, fr.PixelBytes[i+pxi])
		}

		pxRet = append(pxRet, tmpPxl)

		if float64(xCounter)/3.0 == float64(fr.Width) {
			yCounter++
			xCounter = 0
		}

	}

	return pxRet
}

type Detection struct {
	Target      string //Special type 'motion'. See below. All others are the names of detectors
	TopLeft     image.Point
	BottomRight image.Point
	Percent     float64
}

// just basic percent of pixels that changed beyond the threshold
func (fr *Frame) DetectMotionGlobal(reference Frame, threshold int) error {

	if len(fr.PixelBytes) != len(reference.PixelBytes) {
		return fmt.Errorf("frame byte lengths are not equal: %d %d", len(fr.PixelBytes), len(reference.PixelBytes))
	} else if fr.ChannelDepth != reference.ChannelDepth {
		return fmt.Errorf("channel depths don't match for fr/reference comparison: %d %d", fr.ChannelDepth, reference.ChannelDepth)
	}

	pixDiffCount := 0
	tmpDet := Detection{}
	byteDiffCount := 0

	for cnt := range fr.PixelBytes {

		//Hopefully, checking for non-equal and then doing the math for exact basdiff later helps wih performance
		if fr.PixelBytes[cnt] != reference.PixelBytes[cnt] {

			absDiff := 0
			frb := int(fr.PixelBytes[cnt])
			ref := int(reference.PixelBytes[cnt])

			if frb > ref {
				absDiff = frb - ref
			} else {
				absDiff = ref - frb
			}
			byteDiffCount += absDiff
			pixDiffCount += 1
		}
	}

	//pixDiffCount = float64(byteDiffCount) / float64(fr.ChannelDepth)

	//TODO: width and height need to be checked for down-conversion to point types

	tmpDet.Target = "motion"
	tmpDet.Percent = getPerc(pixDiffCount, len(fr.PixelBytes), fr.ChannelDepth)
	tmpDet.TopLeft.X = 0
	tmpDet.TopLeft.Y = 0
	tmpDet.BottomRight.X = int(fr.Width)
	tmpDet.BottomRight.Y = int(fr.Height)

	fr.Detections = append(fr.Detections, tmpDet)

	return nil
}

func getPerc(diffs, lenPix int, chans uint8) float64 {
	pixelCount := float64(lenPix) / float64(chans)
	return float64(diffs) / pixelCount
}

func (fr *Frame) GetCamAndOwner() string {
	cam := fr.CamId
	usr := fr.Owner
	return cam + usr
}

// blend two frames
func (fr *Frame) BlendIn(nput Frame, method string) error {
	if len(nput.PixelBytes) != len(fr.PixelBytes) {
		return fmt.Errorf("can't blend these images. Pixel byte lengths do not match nput: %d fr: %d", len(nput.PixelBytes), len(fr.PixelBytes))
	} else if fr.ChannelDepth != nput.ChannelDepth {
		return fmt.Errorf("cant blend images. Channel depths do not match: nput: %d fr: %d", nput.ChannelDepth, fr.ChannelDepth)
	}

	for pxCount := range fr.PixelBytes {
		fr.PixelBytes[pxCount] = averageTwo(fr.PixelBytes[pxCount], nput.PixelBytes[pxCount])
	}

	return nil
}

func averageTwo(px1, px2 byte) byte {
	cvt1 := int(px1)
	cvt2 := int(px1)

	comb := cvt2 + cvt1
	return byte(int(comb / 2))
}
