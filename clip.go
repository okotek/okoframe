package okoframe

import (
	"fmt"
	"sort"

	"gocv.io/x/gocv"
)

type Clip struct {
	Rasters  []RasterMap
	MetaData ClipMD
}

func (c *Clip) SetOwner(owner string) {
	c.MetaData.Owner = owner
}

func (c *Clip) SetTimestamps() {

}

func (c *Clip) SetFrameTimes() {

}

func (c *Clip) SetRasters(rasters []RasterMap) {

}

type ClipMD struct {
	Id              string
	TimeStamps      []uint64 //Each ordered Frame's time stamp
	Owner           string
	FrameTimeLength uint64 //How long each frame is displayed during playback
}

type RasterMap struct {
	PixelList []Pixel
	Channels  uint16
	Width     uint32
	Height    uint32
	CVType    gocv.MatType
}

func (f *Frame) FrameToRasterMap() RasterMap {
	return RasterMap{
		PixelList: f.PixelList,
		Channels:  f.ChannelDepth,
		Width:     f.Width,
		Height:    f.Height,
		CVType:    f.OpenCVType,
	}
}

/*
func ClipFromFrames(frameList []Frame) (Clip, error) {

	var storeWid uint64 = 0
	var storeHei uint64 = 0
	var storeChan uint8 = 0
	var storeOwner string = ""
	var storeCam string = ""
	var rastMap []RasterMap = []RasterMap{}

	var oldest uint64 = 0
	var newest uint64 = len(frameList)
	var oldestStamp uint64 = 0
	var newestStamp uint64 = 0

	var dimensionNonMatch error = fmt.Errorf("dimensions don't match. Failed")

	var retClip Clip = Clip{
		genId(),
		0,
		"",
		0,
		"",
		0,
		rastMap,
	}

	for cnt, cfrm := range frameList {
		if cnt == 0 {
			storeWid = cfrm.Width
			storeHei = cfrm.Height
		} else if storeWid != cfrm.Width || storeHei != cfrm.Height {
			return Clip{}, fmt.Errorf("%s %d vs %d and %d vs %d", dimensionNonMatch, storeHei, cfrm.Height, storeWid, cfrm.Width)
		}

	}

	for count, cfrm := range frameList {
		if count == 0 {
			storeWid = cfrm.Width
			storeHei = cfrm.Height
			storeChan = cfrm.ChannelDepth
			storeOwner = cfrm.Owner
			storeCam = cfrm.CamId
		}
	}

	for count, cfrm := range frameList {
		if count == 0 {
			oldest = cfrm.TimeStamp
			newest = cfrm.TimeStamp
		}

		if cfrm.TimeStamp > newest {
			newest = uint64(count)
			newestStamp = cfrm.TimeStamp
		}

		if cfrm.TimeStamp < oldest {
			oldest = uint64(count)
			oldestStamp = cfrm.TimeStamp
		}
	}

	retClip.StartFrameId = frameList[oldest].Id
	retClip.EndFrameId = frameList[newest].Id

	retClip.TimeStampStart = frameList[oldest].TimeStamp
	retClip.TimeStampStop = frameList[newest].TimeStamp

	retClip.FrameTimeLength = uint64((retClip.TimeStampStop - retClip.TimeStampStart) / uint64(len(frameList)))

	return retClip, nil

}
*/
/*
func ClipFromFrames(frameList []Frame) (Clip, error) {

	if len(frameList) < 2 {
		return Clip{}, fmt.Errorf("frameList is too short")
	}

	// Initialize variables to track oldest and newest frames
	oldestFrame := frameList[0].Id
	newestFrame := frameList[0].Id
	oldestTimeStamp := frameList[0].TimeStamp
	newestTimeStamp := frameList[0].TimeStamp

	// Loop through the frameList to find the oldest and newest frames
	for _, frame := range frameList {
		if frame.TimeStamp < oldestTimeStamp {
			oldestTimeStamp = frame.TimeStamp
			oldestFrame = frame.Id
		}
		if frame.TimeStamp > newestTimeStamp {
			newestTimeStamp = frame.TimeStamp
			newestFrame = frame.Id
		}
	}

	// Create and return the Clip
	clip := Clip{
		Id:             oddstring.RandStringSimple(25), // Generate or assign a Clip ID as needed
		TimeStampStart: oldestTimeStamp,                // Start timestamp from the oldest frame
		StartFrameId:   oldestFrame,                    // Oldest frame ID
		TimeStampStop:  newestTimeStamp,                // Stop timestamp from the newest frame
		EndFrameId:     newestFrame,                    // Newest frame ID
		// FrameTimeLength: Set the frame time length if available
		Raster: []RasterMap{}, // Populate as needed based on your logic
	}

	return clip, nil
}
*/

func FramesToCLip(frames []Frame) ([]Frame, error) {
	clipOwner := ""
	var ret []Frame = make([]Frame, 0, len(frames))

	for n, frame := range frames {
		if n == 0 {
			clipOwner = frame.MetaData.Owner
			continue
		} else {
			if frame.MetaData.Owner != clipOwner {
				return []Frame{}, fmt.Errorf("Frame owners did not match: %v and %v", frame.MetaData.Owner, clipOwner)
			}
		}
	}

	sort.Slice(frames, func(i, j int) bool {
		return frames[i].TimeStamp < frames[j].TimeStamp
	})

}
