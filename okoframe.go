package okoframe

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"gocv.io/x/gocv"
	//"gitlab.com/ashinnv/oddstring"
)

func genId() string {
	alphabet := "abcdefghijklmnopqrstuvwxyz"
	letters := strings.Split(alphabet, "")
	finStr := ""

	for i := 0; i < 55; i++ {
		rNo := rand.Intn(len(letters))
		finStr = finStr + letters[rNo]
	}

	finStr = finStr + strconv.Itoa(int(time.Now().Unix()))

	return finStr
}

type Frame struct {
	Id           string
	TimeStamp    uint64
	Width        uint64 //pixels
	Height       uint64
	ChannelDepth uint8
	OpenCVType   gocv.MatType
	PixelBytes   []byte

	Owner        string
	CamId        string
	FrameVersion string

	Detections []Detection
}

func BuildFrameFromImage(path, owner, camId string) Frame {
	imgMat := gocv.IMRead(path, 1)

	return Frame{
		genId(),
		uint64(time.Now().UnixNano()),
		uint64(imgMat.Cols()),
		uint64(imgMat.Rows()),
		uint8(imgMat.Channels()),
		imgMat.Type(),
		imgMat.ToBytes(),

		owner,
		camId,
		"0.1",

		[]Detection{},
	}
}

// Saves file, returns string name of file saved
func (fr Frame) SaveToDisk() (string, error) {
	var buf bytes.Buffer
	file, createError := os.Create(fr.Id + ".oframe")
	if createError != nil {
		fmt.Println("Failed to create file: ", createError)
	}
	defer file.Close()

	encErr := gob.NewEncoder(&buf).Encode(fr)
	if encErr != nil {
		fmt.Println("Failed to encode data: ", encErr)
		return "", encErr
	}

	_, writeErr := file.Write(buf.Bytes())
	if writeErr != nil {
		fmt.Println("Failed to write data: ", writeErr)
		return "", writeErr
	}

	return fr.Id + ".oframe", nil
}

func ReadFromDisk(fName string) (Frame, error) {

	var templateFrame Frame
	var byteBuffer bytes.Buffer

	fDat, readErr := os.ReadFile(fName)
	if readErr != nil {
		return templateFrame, readErr
	}

	byteBuffer.Write(fDat)

	if decErr := gob.NewDecoder(&byteBuffer).Decode(&templateFrame); decErr != nil {
		return templateFrame, decErr
	} else {
		return templateFrame, nil
	}

}
